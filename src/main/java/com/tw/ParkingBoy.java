package com.tw;

public class ParkingBoy {
    private final ParkingLot parkingLot;
    private String lastErrorMessage;

    public ParkingBoy(ParkingLot parkingLot) {
        this.parkingLot = parkingLot;
    }

    public ParkingTicket park(Car car) {
        // TODO: Implement the method according to test
        // <-start-
        ParkingTicket ticket = new ParkingTicket();

        if (parkingLot.getAvailableParkingPosition() == 0) {
            lastErrorMessage = parkingLot.park(car).getMessage();
            ticket = null;
        }
        ticket = parkingLot.park(car).getTicket();

        return ticket;
        // ---end->
    }

    public Car fetch(ParkingTicket ticket) {
        // TODO: Implement the method according to test
        // <-start-
        FetchingResult fetching = parkingLot.fetch(ticket);

        if (!fetching.isSuccess()) {
            lastErrorMessage = parkingLot.fetch(ticket).getMessage();
        }

        return fetching.getCar();

        // ---end->
    }

    public String getLastErrorMessage() {
        return lastErrorMessage;
    }
}
